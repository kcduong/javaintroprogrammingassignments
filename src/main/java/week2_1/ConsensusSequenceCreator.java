/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week2_1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {
    ///add public iupac, sequences etc. variables

    /**
     * testing main.
     * @param args
     */
    public static HashMap<String,String> iupacList = new HashMap<>();

    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {
        /// add instance variables? this.etc

        //Fill the Hashmap
        iupacList.put("A","A");
        iupacList.put("C","C");
        iupacList.put("G","G");
        iupacList.put("T","T");
        iupacList.put("U","U");
        iupacList.put("AT","W");
        iupacList.put("CG","S");
        iupacList.put("AC","M");
        iupacList.put("GT","K");
        iupacList.put("AG","R");
        iupacList.put("CT","Y");
        iupacList.put("CGT","B");
        iupacList.put("AGT","D");
        iupacList.put("ACT","H");
        iupacList.put("ACG","V");
        iupacList.put("ACGT","N");

        //Look for varieties between sequences (nieuwe method)
        ArrayList<String> variationList = new ArrayList<>();
        for (int i = 0; i < sequences[0].length(); i++) {
            ArrayList<Character> variation = new ArrayList<>();
            //(nieuwe method)
            for (int j = 0; j < sequences.length; j++) {
                if(!variation.contains(sequences[j].charAt(i))){
                    variation.add(sequences[j].charAt(i));
                }
            }
            //Sort variations alphabetically before adding to list
            Collections.sort(variation);
            //System.out.println(variation);
            String var = "";
            for(Character c : variation){
                var += c.toString();
            }
            variationList.add(var);
        }

        //iupac version or not
        ArrayList<String> resultList = new ArrayList<>();
        if(iupac){
            for (String v: variationList) {
                resultList.add(iupacList.get(v));
            }
        }
        else{
            for (int i = 0; i < variationList.size(); i++) {
                if(variationList.get(i).length() == 1){
                    resultList.add(variationList.get(i));
                }
                else{
                    resultList.add("[" + variationList.get(i).replace("","/").replaceAll("\\/$|^\\/", "") + "]");
                }
            }
        }
        //System.out.println(resultList);
        String result = String.join("",resultList);
        System.out.println(result);

        return result;
    }
}